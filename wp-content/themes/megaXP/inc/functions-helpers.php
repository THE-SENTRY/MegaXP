<?php 
// HELPER METHODS AND FUNCTIONS //////////////////////////////////////////////////////

	/**
	 * Print the <title> tag based on what is being viewed.
	 * @return string
	 */
	function print_title(){
		global $page, $paged;

		wp_title( '|', true, 'right' );
		bloginfo( 'name' );

		// Add a page number if necessary
		if ( $paged >= 2 || $page >= 2 ){
			echo ' | ' . sprintf( __( 'Página %s' ), max( $paged, $page ) );
		}
	}

	/**
	 * Print the <body> tag based on what is being viewed.
	 * @return string
	 */
	function print_description(){
		global $post;
		if(isset($post->post_content) AND ! is_post_type_archive() AND ! is_page('videos')){
			echo wp_trim_words(getDescriptionVideo($post->ID, $post->post_content), 30);
		}else{
			bloginfo( 'description' );
		}
	}

	/**
	 * Print the <image> tag based on what is being viewed.
	 * @return string
	 */
	function print_image(){
		global $post;
		if(isset($post->ID) AND ! is_post_type_archive() AND ! is_page('videos') ){
			$duration = get_post_meta($post->ID, '_duration_video', true);
			echo ($duration != '') ? getImageVideo($post->ID) : getImageGeneral($post->ID);
		}else{
			echo get_template_directory_uri().'/images/logo.png';
		}
	}

	/**
	 * Regresa la url del attachment especificado
	 * @param  int     $post_id
	 * @param  string  $size
	 * @return string  url de la imagen
	 */
	function attachment_image_url($post_id, $size){
		$image_id   = get_post_thumbnail_id($post_id);
		$image_data = wp_get_attachment_image_src($image_id, $size, true);
		return isset($image_data[0]) ? $image_data[0] : '';
	}

	/**
	 * EDITA EL FORMATO DE LA FECHA
	 */
	function getDateTransform($post_date){
		$dia = substr($post_date, 8, 2);
		$mes = substr($post_date, 5, 2);
		$ano = substr($post_date, 0, 4);

		$nameOfDay = date('D', strtotime($post_date));
		$diasTranslate = array('Sun' => 'Domingo', 'Mon' => 'Lunes', 'Tues' => 'Martes', 'Wed' => 'Miércoles', 'Thurs' => 'Jueves', 'Fri' => 'Viernes', 'Sat' => 'Sábado');

		$meses = array('01' => 'Enero', '02' => 'febrero', '03' => 'Marzo', '04' => 'abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');

		return $diasTranslate[$nameOfDay].' '.$dia.' de '.$meses[$mes];
	}

	/**	
	 * PAGINACIONEXPOSICIONES
	 * @return [string]         [html con la paginacion]
	 */
	function pagenavi($paged = '', $num_pages = '', $siteUrl = '', $especial = false, $simbol_url = '?') {
	    global $wpdb, $wp_query;
	    
	    $before = '';
	    $after = '';

	    $pagenavi_options = optionsPagination();

	    if (!is_single()) {

	        $paged = $paged == '' ? intval(get_query_var('paged')) : $paged;
	        $max_page = $num_pages == '' ? $wp_query->max_num_pages : $num_pages;
	 
	        if(empty($paged) || $paged == 0) {
	            $paged = 1;
	        }
	 
	        $pages_to_show = intval($pagenavi_options['num_pages']);
	        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
	        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
	        $pages_to_show_minus_1 = $pages_to_show - 1;
	        $half_page_start = floor($pages_to_show_minus_1/2);
	        $half_page_end = ceil($pages_to_show_minus_1/2);
	        $start_page = $paged - $half_page_start;
	 
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	 
	        $end_page = $paged + $half_page_end;
	        if(($end_page - $start_page) != $pages_to_show_minus_1) {
	            $end_page = $start_page + $pages_to_show_minus_1;
	        }
	        if($end_page > $max_page) {
	            $start_page = $max_page - $pages_to_show_minus_1;
	            $end_page = $max_page;
	        }
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	 
	        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
	        $larger_start_page_start = (round_num($start_page, 10) + $larger_page_multiple) - $larger_per_page;
	        $larger_start_page_end = round_num($start_page, 10) + $larger_page_multiple;
	        $larger_end_page_start = round_num($end_page, 10) + $larger_page_multiple;
	        $larger_end_page_end = round_num($end_page, 10) + ($larger_per_page);
	 
	        if($larger_start_page_end - $larger_page_multiple == $start_page) {
	            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
	            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
	        }
	        if($larger_start_page_start <= 0) {
	            $larger_start_page_start = $larger_page_multiple;
	        }
	        if($larger_start_page_end > $max_page) {
	            $larger_start_page_end = $max_page;
	        }
	        if($larger_end_page_end > $max_page) {
	            $larger_end_page_end = $max_page;
	        }
	        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {

	            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
	            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
	            echo $before.'<ul class="pagination blog__pagination col-xs-12 text-center">'."\n";
	 

	        	if ($especial == true) {
	        		$pa = $paged - 1;
	        		echo $paged > 1 ? '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$pa.'">Anterior</a></li>' : '';
	        	}

	            if ($start_page >= 2 && $pages_to_show < $max_page) {
	                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
	                
	                echo '<li><a href="'.$siteUrl.$simbol_url.'" title="'.$first_page_text.'">1</a></li>';
	                
	            }
	 
	            if($larger_page_to_show > 0 && $larger_start_page_start > 0 && $larger_start_page_end <= $max_page) {
	                for($i = $larger_start_page_start; $i < $larger_start_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<li><a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	 
	            for($i = $start_page; $i  <= $end_page; $i++) {
	                if($i == $paged) {
	                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
	                    echo '<li><a class="active">'.$current_page_text.'</a></li>';
	                } else {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    $url = $especial == true ? $siteUrl.$simbol_url.'pagina='.$i : esc_url(get_pagenum_link($i));
	                    echo '<li><a href="'.$url.'" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	 
	            if ($end_page < $max_page) {
	                
	                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
	                echo '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$max_page.'" title="'.$last_page_text.'">'.$max_page.'</a></li>';
	            }
	        	if ($especial == true) {
	        		$pa = $paged + 1;
	        		echo $paged < $num_pages ? '<li><a href="'.$siteUrl.$simbol_url.'pagina='.$pa.'">Siguiente</a></li>' : '';
	        	}
	            if($larger_page_to_show > 0 && $larger_end_page_start < $max_page) {
	                for($i = $larger_end_page_start; $i <= $larger_end_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<li><a href="'.esc_url(get_pagenum_link($i)).'" title="'.$page_text.'">'.$page_text.'</a></li>';
	                }
	            }
	            echo '</ul>'.$after."\n";
	        }
	    }
	}

	function round_num($num, $to_nearest) {
	   /*Round fractions down (http://php.net/manual/en/function.floor.php)*/
	   return floor($num/$to_nearest)*$to_nearest;
	}

	/**	
	 * OPCIONES PARA LA PAGINACION
	 * @return [type] [description]
	 */
	function optionsPagination(){
		$pagenavi_options = array();
	    $pagenavi_options['pages_text'] = ('Página %CURRENT_PAGE% de %TOTAL_PAGES%:');
	    $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['first_text'] = ('Primera');
	    $pagenavi_options['last_text'] = ('Última');
	    $pagenavi_options['next_text'] = 'Siguiente';
	    $pagenavi_options['prev_text'] = 'Anterior';
	    $pagenavi_options['dotright_text'] = '...';
	    $pagenavi_options['dotleft_text'] = '...';
	    $pagenavi_options['num_pages'] = 10; //continuous block of page numbers
	    $pagenavi_options['always_show'] = 0;
	    $pagenavi_options['num_larger_page_numbers'] = 0;
	    $pagenavi_options['larger_page_numbers_multiple'] = 5;

	    return $pagenavi_options;
	}

	/**
	 * IMAGE MOBILE HOME HEADER
	 * @return [type] [description]
	 */
	function imageMobileHome($homeId){
		$image_id = get_post_meta( $homeId, '_mobile_home_image_id', true );
		if ( $image_id && get_post( $image_id ) ) {
			$image = wp_get_attachment_image_src( $image_id, 'home_header' );
			return isset($image[0]) ? $image[0] : '';
		}

		return attachment_image_url($homeId, 'home_header');
	}

