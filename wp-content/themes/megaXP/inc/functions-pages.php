<?php


// CUSTOM PAGES //////////////////////////////////////////////////////////////////////
add_action('init', function(){


	// ¿Qué es MEGA XP?
	if( ! get_page_by_path('que-es-mega-xp') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => '¿Qué es MEGA XP?',
			'post_name'   => 'que-es-mega-xp',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// ¿Qué es MEGA XP?
	if( ! get_page_by_path('por-que-una-convencion-de-juegos-de-mesa') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => '¿Por qué una convención de juegos de mesa?',
			'post_name'   => 'por-que-una-convencion-de-juegos-de-mesa',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// DONDE
	if( ! get_page_by_path('donde') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => '¿Donde?',
			'post_name'   => 'donde',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// BOLETOS
	if( ! get_page_by_path('boletos') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Boletos',
			'post_name'   => 'boletos',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// BLOG
	if( ! get_page_by_path('blog') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Blog',
			'post_name'   => 'blog',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// Aviso de privacidad
	if( ! get_page_by_path('aviso-de-privacidad') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Aviso de privacidad',
			'post_name'   => 'aviso-de-privacidad',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// Aviso de privacidad
	if( ! get_page_by_path('header-home') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Header home',
			'post_name'   => 'header-home',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// Newsletter
	if( ! get_page_by_path('newsletter') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Newsletter',
			'post_name'   => 'newsletter',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}

	// Header Expositores
	if( ! get_page_by_path('header-expositores') ){
		$page = array(
			'post_author' => 1,
			'post_status' => 'publish',
			'post_title'  => 'Header Expositores',
			'post_name'   => 'header-expositores',
			'post_type'   => 'page'
		);
		wp_insert_post( $page, true );
	}
});