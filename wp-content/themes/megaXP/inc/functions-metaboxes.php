<?php


// CUSTOM METABOXES //////////////////////////////////////////////////////////////////

	add_action('add_meta_boxes', function(){
		global $post;
		add_meta_box( 'metabox_actividades', 'Información extra', 'acs_info_actividades', 'actividades', 'side', 'high' );
		add_meta_box( 'metabox_torneos', 'Información extra', 'acs_info_torneos', 'torneos', 'side', 'high' );
		add_meta_box( 'metabox_expositores', 'Información extra', 'acs_info_expositores', 'expositores', 'side', 'high' );

		if($post->post_name == 'header-home'){
			add_meta_box( 'metabox_page', 'Subtitulo', 'acs_subtitulo_home', 'page', 'side', 'high' );
		}

		if($post->post_name == 'boletos'){
			add_meta_box( 'metabox_page', 'Link', 'acs_link_boletos', 'page', 'side', 'high' );
		}

		if($post->post_name == 'donde'){
			add_meta_box( 'metabox_page', 'Información extra', 'acs_donde_direccion', 'page', 'side', 'high' );
		}

		if($post->post_name == 'header-home'){
			add_meta_box( 'metabox_page', 'Imagen destacada mobile', 'acs_home_featured_image', 'page', 'side' );
		}
	});


// CUSTOM METABOXES CALLBACK FUNCTIONS ///////////////////////////////////////////////

	function acs_info_actividades($post){
		wp_nonce_field(__FILE__, '_actividades_nonce'); 
		$fecha = get_post_meta($post->ID, '_fecha_actividad', true);
		$hora = get_post_meta($post->ID, '_hora_actividad', true);
		$zona = get_post_meta($post->ID, '_zona_actividad', true); ?>

		<label for='hora_actividad' class='label-paquetes'>Hora: </label>
		<input type='text' name='hora_actividad' class='widefat' value='<?php echo $hora; ?>' id='hora_actividad' placeholder="15:00hrs" />

		<label for='zona_actividad' class='label-paquetes'>Zona: </label>
		<input type='text' name='zona_actividad' class='widefat' value='<?php echo $zona; ?>' id='zona_actividad'/>

	<?php }

	function acs_info_torneos($post){
		wp_nonce_field(__FILE__, '_torneos_nonce'); 
		$fecha = get_post_meta($post->ID, '_fecha_torneo', true);
		$hora = get_post_meta($post->ID, '_hora_torneo', true);
		$zona = get_post_meta($post->ID, '_zona_torneo', true);
		$premio = get_post_meta($post->ID, '_premio_torneo', true); ?>

		<label for='hora_torneo' class='label-paquetes'>Hora: </label>
		<input type='text' name='hora_torneo' class='widefat' value='<?php echo $hora; ?>' id='hora_torneo' placeholder="15:00hrs" />

		<label for='zona_torneo' class='label-paquetes'>Zona: </label>
		<input type='text' name='zona_torneo' class='widefat' value='<?php echo $zona; ?>' id='zona_torneo'/>

		<label for='premio_torneo' class='label-paquetes'>Premio: </label>
		<input type='text' name='premio_torneo' class='widefat' value='<?php echo $premio; ?>' id='premio_torneo'  placeholder="1,000MX"/>

	<?php }

	function acs_info_expositores($post){
		wp_nonce_field(__FILE__, '_expositores_nonce'); 
		$url_expositor = get_post_meta($post->ID, '_url_expositor', true); ?>

		<label for='url_expositor' class='label-paquetes'>Link expositor
			: </label>
		<input type='text' name='url_expositor' class='widefat' value='<?php echo $url_expositor; ?>' id='url_expositor' placeholder="http://link.com" />

	<?php }

	function acs_subtitulo_home($post){
		wp_nonce_field(__FILE__, '_pages_home_nonce'); 
		$subtitle_home = get_post_meta($post->ID, '_subtitle_home', true); ?>

		<input type='text' name='subtitle_home' class='widefat' value='<?php echo $subtitle_home; ?>' id='subtitle_home' />
	<?php }

	function acs_link_boletos($post){
		wp_nonce_field(__FILE__, '_pages_boletos_nonce'); 
		$link_boletos = get_post_meta($post->ID, '_link_boletos', true); ?>

		<input type='text' name='link_boletos' class='widefat' value='<?php echo $link_boletos; ?>' id='link_boletos' />
	<?php }

	function acs_donde_direccion($post){
		wp_nonce_field(__FILE__, '_pages_donde_nonce'); 
		$direccion = get_post_meta($post->ID, '_direccion_evento', true);
		
		$lat = get_post_meta($post->ID, '_lat_evento', true);
		$long = get_post_meta($post->ID, '_long_evento', true);?>

		<label for='direccion_evento' class='label-paquetes'>Dirección: </label>
		<div class="data">
			<input type="text" id="addresscolaborador" class="widefat" name="direccion_evento" value="<?php echo $direccion; ?>" />
			<input type="hidden" class="" name="lat_evento" id="lat_evento" value="<?php echo $lat; ?>" />
			<input type="hidden" class="" name="long_evento" id="long_evento" value="<?php echo $long; ?>" />
		</div>
		<br>
		<div class="iframe-cont">
			<?php if ($lat != '') : ?>
				<iframe width="100%" height="170" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $lat; ?>,<?php echo $long; ?>&hl=es;z=14&amp;output=embed"></iframe>
			<?php endif; ?>
		</div> 
	<?php }

	function acs_home_featured_image($post){
		global $content_width, $_wp_additional_image_sizes;
		$image_id = get_post_meta( $post->ID, '_mobile_home_image_id', true );
		$old_content_width = $content_width;
		$content_width = 254;
		$content = '<div id="content-img-home">';
		if ( $image_id && get_post( $image_id ) ) {
			if ( ! isset( $_wp_additional_image_sizes['medium'] ) ) {
				$thumbnail_html = wp_get_attachment_image( $image_id, array( $content_width, $content_width ) );
			} else {
				$thumbnail_html = wp_get_attachment_image( $image_id, 'medium' );
			}
			if ( ! empty( $thumbnail_html ) ) {
				$content .= $thumbnail_html;
				$content .= '<p class="hide-if-no-js"><a href="javascript:;" id="remove_listing_image_button" >' . esc_html__( 'Eliminar imagen destacada', 'text-domain' ) . '</a></p>';
				$content .= '<input type="hidden" id="upload_listing_image" name="_mobile_home_cover_image" value="' . esc_attr( $image_id ) . '" />';
			}
			$content_width = $old_content_width;
		} else {
			$content = '<img src="" style="width:' . esc_attr( $content_width ) . 'px;height:auto;border:0;display:none;" />';
			$content .= '<p class="hide-if-no-js"><a title="' . esc_attr__( 'Añadir imagen destacada', 'text-domain' ) . '" href="javascript:;" id="upload_listing_image_button" id="set-listing-image" data-uploader_title="' . esc_attr__( 'Elige una imagen', 'text-domain' ) . '" data-uploader_button_text="' . esc_attr__( 'Añadir imagen destacada', 'text-domain' ) . '">' . esc_html__( 'Añadir imagen destacada', 'text-domain' ) . '</a></p>';
			$content .= '<input type="hidden" id="upload_listing_image" name="_mobile_home_cover_image" value="" />';
		}
		$content .= '</div>';
		echo $content;
	}
	

// SAVE METABOXES DATA ///////////////////////////////////////////////////////////////


	add_action('save_post', function($post_id){


		if ( ! current_user_can('edit_page', $post_id)) 
			return $post_id;


		if ( defined('DOING_AUTOSAVE') and DOING_AUTOSAVE ) 
			return $post_id;
		
		
		if ( wp_is_post_revision($post_id) OR wp_is_post_autosave($post_id) ) 
			return $post_id;


		if ( isset($_POST['hora_actividad']) and check_admin_referer(__FILE__, '_actividades_nonce') ){
			update_post_meta($post_id, '_hora_actividad', $_POST['hora_actividad']);
			update_post_meta($post_id, '_zona_actividad', $_POST['zona_actividad']);
		}

		if ( isset($_POST['hora_torneo']) and check_admin_referer(__FILE__, '_torneos_nonce') ){
			update_post_meta($post_id, '_hora_torneo', $_POST['hora_torneo']);
			update_post_meta($post_id, '_zona_torneo', $_POST['zona_torneo']);
			update_post_meta($post_id, '_premio_torneo', $_POST['premio_torneo']);
		}

		if ( isset($_POST['url_expositor']) and check_admin_referer(__FILE__, '_expositores_nonce') ){
			update_post_meta($post_id, '_url_expositor', $_POST['url_expositor']);
		}

		if ( isset($_POST['subtitle_home']) and check_admin_referer(__FILE__, '_pages_home_nonce') ){
			update_post_meta($post_id, '_subtitle_home', $_POST['subtitle_home']);
		}

		if ( isset($_POST['link_boletos']) and check_admin_referer(__FILE__, '_pages_boletos_nonce') ){
			update_post_meta($post_id, '_link_boletos', $_POST['link_boletos']);
		}

		if ( isset($_POST['direccion_evento']) and check_admin_referer(__FILE__, '_pages_donde_nonce') ){
			update_post_meta($post_id, '_direccion_evento', $_POST['direccion_evento']);
			update_post_meta($post_id, '_lat_evento', $_POST['lat_evento']);
			update_post_meta($post_id, '_long_evento', $_POST['long_evento']);
		}

	});

	add_action( 'save_post', 'mobile_home_image_save', 10, 1 );
	function mobile_home_image_save ( $post_id ) {
		if( isset( $_POST['_mobile_home_cover_image'] ) ) {
			$image_id = (int) $_POST['_mobile_home_cover_image'];
			update_post_meta( $post_id, '_mobile_home_image_id', $image_id );
		}
	}

// USERS META

	/**
	 * Adds additional user fields
	 */
	 
	function additional_user_fields( $user ) { ?>
	 
	    <h3><?php _e( 'Additional User Meta', 'textdomain' ); ?></h3>
	 
	    <table class="form-table">
	 
	        <tr>
	            <th><label for="user_meta_image"><?php _e( 'A special image for each user', 'textdomain' ); ?></label></th>
	            <td>
	                <input type='button' class="additional-user-image button-primary" value="<?php _e( 'Añadir imagen', 'textdomain' ); ?>" id="add-image-profile"/><br />
	            </td>
	        </tr>
	 
	    </table><!-- end form-table -->
	<?php } // additional_user_fields
	 
	add_action( 'show_user_profile', 'additional_user_fields' );
	add_action( 'edit_user_profile', 'additional_user_fields' );
