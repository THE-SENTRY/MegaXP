<?php

// CUSTOM POST TYPES /////////////////////////////////////////////////////////////////

/**
 * Rename "Posts" to "Blog"
 */
add_action( 'admin_menu', 'acs_change_post_menu_label' );
add_action( 'init', 'acs_change_post_object_label' );
function acs_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Blog';
	$submenu['edit.php'][5][0] = 'Blog';
	$submenu['edit.php'][10][0] = 'Agregar Artículo';
	$submenu['edit.php'][16][0] = 'Blog Tags';
	echo '';
}

function acs_change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Blog';
	$labels->singular_name = 'Blog';
	$labels->add_new = 'Agregar Artículo';
	$labels->add_new_item = 'Agregar Artículo';
	$labels->edit_item = 'Editar Artículo';
	$labels->new_item = 'Blog';
	$labels->view_item = 'Ver Artículo';
	$labels->search_items = 'Buscar artículos';
	$labels->not_found = 'No existe el artículo';
	$labels->not_found_in_trash = 'No se encontró ningun articulo en la Papelera';
}

/**	
 * CREATE POST TYPES
 */
function acs_post_types() {

	// Post-type Expositores
	$labels = array(
		'name'				=> _x( 'Expositores', 'post type general name' ),
		'singular_name'		=> _x( 'Expositor', 'post type singular name' ),
		'menu_name'			=> _x( 'Expositores', 'admin menu' ),
		'name_admin_bar'	=> _x( 'Expositor', 'add new on admin bar' ),
		'add_new'			=> _x( 'Añadir nuevo', 'book' ),
		'add_new_item'		=> __( 'Añadir nuevo expositor' ),
		'new_item'			=> __( 'Nuevo expositor' ),
		'edit_item'			=> __( 'Editar expositor' ),
		'view_item'			=> __( 'Ver expositor' ),
		'search_items'		=> __( 'Buscar expositores' ),
		'not_found'			=> __( 'No se encontraron expositores.' ),
	);

	$args = array(
		'labels'		=> $labels,
		'public'		=> true,
		'label'			=> 'Expositores',
		'supports'		=> ['title', 'thumbnail', 'excerpt', 'revisions'],
		'menu_position'	=> 5,
		'menu_icon'		=> 'dashicons-pressthis',
		'has_archive'	=> true,
		'rewrite'		=> ['slug' => 'expositores']
	);

	register_post_type( 'expositores', $args );

	// Post-type Invitados
	$labels = array(
		'name'				=> _x( 'Invitados', 'post type general name' ),
		'singular_name'		=> _x( 'Invitado', 'post type singular name' ),
		'menu_name'			=> _x( 'Invitados', 'admin menu' ),
		'name_admin_bar'	=> _x( 'Invitado', 'add new on admin bar' ),
		'add_new'			=> _x( 'Añadir nuevo', 'book' ),
		'add_new_item'		=> __( 'Añadir nuevo invitado' ),
		'new_item'			=> __( 'Nuevo invitado' ),
		'edit_item'			=> __( 'Editar invitado' ),
		'view_item'			=> __( 'Ver invitado' ),
		'search_items'		=> __( 'Buscar invitados' ),
		'not_found'			=> __( 'No se encontraron invitados.' ),
	);

	$args = array(
		'labels'		=> $labels,
		'public'		=> true,
		'label'			=> 'Invitados',
		'supports'		=> ['title', 'thumbnail', 'excerpt', 'revisions'],
		'menu_position'	=> 5,
		'menu_icon'		=> 'dashicons-image-filter',
		'has_archive'	=> true,
		'rewrite'		=> ['slug' => 'invitados']
	);

	register_post_type( 'invitados', $args );

	// Post-type  ACTIVIDADEs
	$labels = array(
		'name'				=> _x( 'Actividades', 'post type general name' ),
		'singular_name'		=> _x( 'Actividad', 'post type singular name' ),
		'menu_name'			=> _x( 'Actividades', 'admin menu' ),
		'name_admin_bar'	=> _x( 'Actividad', 'add new on admin bar' ),
		'add_new'			=> _x( 'Añadir nueva', 'book' ),
		'add_new_item'		=> __( 'Añadir nueva actividad' ),
		'new_item'			=> __( 'Nuevo actividad' ),
		'edit_item'			=> __( 'Editar actividad' ),
		'view_item'			=> __( 'Ver actividad' ),
		'search_items'		=> __( 'Buscar actividades' ),
		'not_found'			=> __( 'No se encontraron actividades.' ),
	);

	$args = array(
		'labels'		=> $labels,
		'public'		=> true,
		'label'			=> 'Actividades',
		'supports'		=> ['title', 'excerpt', 'revisions'],
		'menu_position'	=> 5,
		'menu_icon'		=> 'dashicons-welcome-widgets-menus',
		'has_archive'	=> true,
		'rewrite'		=> ['slug' => 'actividades']
	);

	register_post_type( 'actividades', $args );


	// Post-type TORNEOS
	$labels = array(
		'name'				=> _x( 'Torneos', 'post type general name' ),
		'singular_name'		=> _x( 'Torneo', 'post type singular name' ),
		'menu_name'			=> _x( 'Torneos', 'admin menu' ),
		'name_admin_bar'	=> _x( 'Torneo', 'add new on admin bar' ),
		'add_new'			=> _x( 'Añadir nuevo', 'book' ),
		'add_new_item'		=> __( 'Añadir nuevo torneo' ),
		'new_item'			=> __( 'Nuevo torneo' ),
		'edit_item'			=> __( 'Editar torneo' ),
		'view_item'			=> __( 'Ver torneo' ),
		'search_items'		=> __( 'Buscar torneos' ),
		'not_found'			=> __( 'No se encontraron torneos.' ),
	);

	$args = array(
		'labels'		=> $labels,
		'public'		=> true,
		'label'			=> 'Torneos',
		'supports'		=> ['title', 'excerpt', 'revisions'],
		'menu_position'	=> 5,
		'menu_icon'		=> 'dashicons-awards',
		'has_archive'	=> true,
		'taxonomies'    => array( 'fecha' ),
		'rewrite'		=> ['slug' => 'torneos']
	);

	register_post_type( 'torneos', $args );

}

add_action( 'init', 'acs_post_types' );
