<?php
// Initial Setup

date_default_timezone_set('America/Mexico_City');

show_admin_bar(false);

// POST THUMBNAILS SUPPORT ///////////////////////////////////////////////////////////
	
	if ( function_exists('add_theme_support') ){
		add_theme_support('post-thumbnails');
	}

	if ( function_exists('add_image_size') ){
			
		add_image_size( 'home_header', 1500, 300, true );
		add_image_size( 'single', 850, 350, true );
		add_image_size( 'archive', 250, 150, true );


		
		// cambiar el tamaño del thumbnail
		
		update_option( 'medium_large_size_h', 420 );
		update_option( 'medium_large_size_w', 279 );
		update_option( 'medium_large_crop', true );
		
	}



// FRONT END SCRIPTS AND STYLES //////////////////////////////////////////////////////

	function acs_front_scripts() {

		// CSS
		// 
		wp_enqueue_style( 'style', get_stylesheet_uri(), array(), '1.0.0', 'all' );
		
		wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBddaGGte42IgdcdgWYRGM-YOziXx3Y21c', array('jquery'), '1.1.0', true );
		wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array('plugins'), '1.0.0', true );

		// Variables localized
		$object = [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'images_url' => get_template_directory_uri().'/images/',
			'site_url' => site_url(),
		];
		wp_localize_script( 'functions', 'ajax_object', $object);
	}
	add_action( 'wp_enqueue_scripts', 'acs_front_scripts' );



// ADMIN SCRIPTS AND STYLES //////////////////////////////////////////////////////////

	function acs_admin_scripts() {
		wp_enqueue_script('jquery-ui-datepicker');
		
		wp_enqueue_style( 'admin-css', get_template_directory_uri().'/css/admin.css', array(), '1.1.6' );
		wp_enqueue_script( 'maps', 'https://maps.googleapis.com/maps/api/js?libraries=places&language=es-ES&key=AIzaSyBddaGGte42IgdcdgWYRGM-YOziXx3Y21c', array('jquery'), '1.1.0', true );
		wp_enqueue_script( 'functions-admin', get_template_directory_uri() . '/js/admin.js', array('jquery'), '1.1.3', true );	
	}
	add_action( 'admin_enqueue_scripts', 'acs_admin_scripts' );



// CAMBIAR EL CONTENIDO DEL FOOTER EN EL DASHBOARD ///////////////////////////////////

	add_filter( 'admin_footer_text', function() {
		echo 'Creado por <a href="http://alejandrocervantes.me/">Alejandro Cervantes</a>. ';
		echo 'Powered by <a href="http://www.wordpress.org">WordPress</a>';
	});
	