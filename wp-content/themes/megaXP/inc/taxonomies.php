<?php


// TAXONOMIES ////////////////////////////////////////////////////////////////////////

	// Fechas
	if( ! taxonomy_exists('fechas')){

		$labels = array(
			'name'              => 'Fechas',
			'singular_name'     => 'Fecha',
			'search_items'      => 'Buscar',
			'all_items'         => 'Todas',
			'edit_item'         => 'Editar Fecha',
			'update_item'       => 'Actualizar Fecha',
			'add_new_item'      => 'Nuevo Fecha',
			'new_item_name'     => 'Nombre Nuevo Fecha',
			'menu_name'         => 'Fechas'
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'fechas' ),
		);

		register_taxonomy( 'fecha', 'actividades', $args );
	}