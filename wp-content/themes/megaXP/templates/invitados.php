<section id="guests" name="guests" class="container filler-lg-desktop">
	<header class="row">
	    <div class="section-header__divider flex flex-center-x anime">
            <hr class="bg-light left">
            <h1 class="ml9 text-center text-danger headline-xl">
                <span class="letters">Invitados</span>
            </h1>
            <hr class="bg-light right">
        </div>
	</header>
    <div class="row guests-container webkit-scrollbar webkit-scrollbar--light">
        <?php $invitados = new WP_Query( array( 'post_type' => 'invitados', 'posts_per_page' => -1 ) );
        if($invitados->have_posts()):
            while($invitados->have_posts()): $invitados->the_post(); 
                $image_url = attachment_image_url(get_the_ID(), 'medium_large'); ?>
                <div class="col-ss-12 col-xs-6 col-sm-4 col-md-3 guests__card">
                    <img src="<?php echo $image_url; ?>" alt="<?php the_title(); ?>" class="guests__image article-image">
                    <h2 class="headline-rg guests__title text-primary"><?php the_title(); ?></h2>
                    <p class="excerpt-sm text-success-dark excerpt-mobile-sm"><?php echo get_the_excerpt(get_the_ID()) ?></p>
                </div>
            <?php endwhile;
            wp_reset_postdata();
        endif; ?>
    </div>
</section>