<?php $terms = get_terms( array('taxonomy' => 'fecha', 'orderby' => 'name', 'order' => 'ASC', 'hide_empty' => false) );  ?>
<section class="bg-dark text-white filler-lg-desktop" id="activities">
    <div class="container">
        <header class="row">
            <div class="section-header__divider flex flex-center-x anime">
                <hr class="bg-light left">
                <h1 class="ml9 text-center headline-xl">
                    <span class="letters">Actividades</span>
                </h1>
                <hr class="bg-light right">
            </div>
        </header>
        <ul class="list-inline">
            <?php if(!empty($terms)):
                foreach ($terms as $key => $term): ?>
                    <li class="<?php echo $key == 0 ? 'active' : ''; ?>">
                        <a data-toggle="tab" href="#<?php echo $term->slug ?>" class="headline-rg text-white activities__tab undecorated headline-decorated"><span>Ver</span><?php echo getDateTransform($term->name) ?></a>
                    </li>
                <?php endforeach;
            endif; ?>
        </ul>
        <div class="row excerpt-lg wrapper-scroll-gradient">
            <div class="scroll-gradient scroll-gradient--dark tab-content webkit-scrollbar webkit-scrollbar--darker">
                <ul class="col-xs-12 list-inline tab-scroll-mobile activities__static-list">
                    <li class="col-xs-5">
                        <p>Evento</p>
                    </li>
                    <li class="col-xs-2">
                        <p>Hora</p>
                    </li>
                    <li class="col-xs-5">
                        <p>Zona</p>
                    </li>
                </ul>
                <?php if(!empty($terms)):
                    foreach ($terms as $key => $term): ?>
                        <ul id="<?php echo $term->slug ?>" class="col-xs-12 tab-scroll-mobile activities__tab-pane tab-pane fade <?php echo $key == 0 ? ' in active' : ''; ?>">
                            <?php $taxonomy = [
                                [
                                    'taxonomy' => 'fecha',
                                    'field'    => 'term_id',
                                    'terms'    => $term->term_id,
                                ]
                            ];
                            $actividades = new WP_Query( array( 'post_type' => 'actividades', 'tax_query' => $taxonomy, 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key'  => '_hora_actividad', 'order' => 'ASC' ) );

                            if($actividades->have_posts()):
                                while($actividades->have_posts()): $actividades->the_post();
                                    $hora = get_post_meta(get_the_ID(), '_hora_actividad', true);
                                    $zona = get_post_meta(get_the_ID(), '_zona_actividad', true); ?>
                                    <li>
                                        <ul class="row list-inline">
                                            <li class="col-xs-5">
                                                <p><?php the_title(); ?></p>
                                            </li>
                                            <li class="col-xs-2">
                                                <p><?php echo $hora; ?></p>
                                            </li>
                                            <li class="col-xs-5">
                                                <p><?php echo $zona; ?></p>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endwhile;
                                wp_reset_postdata();
                            endif; ?>
                        </ul>
                    <?php endforeach;
                endif; ?>
            </div>
        </div>
    </div>
</section>