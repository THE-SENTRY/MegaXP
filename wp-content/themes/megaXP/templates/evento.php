<section class="container filler-lg-bottom" id="event">
    <div class="row">
        <header class="filler-md-top hidden-xs">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo-horizontal.png" alt="Mega XP" class="section-header__logo spacer-sm-bottom logo-responsive">
        </header>
        <div class="col-sm-6">
            <h2 class="text-info headline-md">¿Qué es Mega XP?</h2>
            <?php $queEs = get_page_by_path('que-es-mega-xp'); ?>
            <div class="text-primary excerpt">
                <?php echo $queEs->post_content; ?>
            </div>
            <?php $convencion = get_page_by_path('por-que-una-convencion-de-juegos-de-mesa'); ?>
            <h2 class="text-info headline-md"><?php echo $convencion->post_title; ?></h2>
            <div class="content-page text-primary excerpt">
                <?php echo wpautop($convencion->post_content); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <?php $donde = get_page_by_path('donde'); 
            $lat = get_post_meta($donde->ID, '_lat_evento', true);
            $long = get_post_meta($donde->ID, '_long_evento', true);?>
            <h2 class="text-info headline-md"><?php echo $donde->post_title; ?></h2>
            <?php if ($lat != '') : ?>
                <iframe width="100%" height="400" class="shadow" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $lat; ?>,<?php echo $long; ?>&hl=es;z=14&amp;output=embed"></iframe>
            <?php endif; ?>
            <div class="content-page text-primary excerpt">
                <?php echo wpautop($donde->post_content); ?>
            </div>
        </div>
    </div>
</section>