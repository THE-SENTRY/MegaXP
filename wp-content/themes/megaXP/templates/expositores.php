<section class="bg-primary box-shadow filler-lg-desktop" id="exhibitors">
	<div class="container">
		<header class="row spacer-sm-bottom">
		    <div class="section-header__divider flex flex-center-x anime">
		        <hr class="bg-light left">
	        	<h1 class="ml9 text-center headline-xl">
	                <span class="letters">Expositores</span>
	            </h1>
		        <hr class="bg-light right">
		    </div>
		</header>
		<div class="row">
			<?php $expositoresPage = get_page_by_path('header-expositores'); ?>
	        <div class="text-center excerpt excerpt-lg excerpt-light spacer-md-bottom excerpt-center">
	        	<?php echo $expositoresPage->post_content; ?>
	    	</div>
	        <div class="grid exhibitors-grid webkit-scrollbar webkit-scrollbar--dark">
	        	<?php $expositores = new WP_Query( array( 'post_type' => 'expositores', 'posts_per_page' => -1 ) );
				if($expositores->have_posts()):
					while($expositores->have_posts()): $expositores->the_post(); 
						$url_expositor = get_post_meta(get_the_ID(), '_url_expositor', true);
						$image_url = attachment_image_url(get_the_ID(), 'full'); ?>
			            <a href="<?php echo $url_expositor; ?>" target="_blank" rel="noopener noreferrer" class="media exhibitors-grid__item">
			            	<div class="media-left media-middle">
			            		<img src="<?php echo $image_url; ?>" alt="exhibitor logo" class="exhibitors__logo">
			            	</div>
			            	<div class="media-body">
			                    <h3 class="media-heading text-warning headline-rg"><?php the_title() ?></h3>
			                    <p class="excerpt-sm text-white"><?php echo get_the_excerpt(get_the_ID()) ?></p>
			                </div>
			            </a>
					<?php endwhile;
					wp_reset_postdata();
				endif; ?>
	            <div class="exhibitors-grid__divider bg-light hidden-xs"></div>
	        </div>
	    </div>
	</div>
</section>