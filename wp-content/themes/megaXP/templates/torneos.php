<?php $terms = get_terms( array('taxonomy' => 'fecha', 'orderby' => 'name', 'order' => 'ASC', 'hide_empty' => false) );  ?>
<section class="box-shadow filler-lg-desktop" id="tournaments">
    <div class="container">
        <header class="row">
            <div class="section-header__divider flex flex-center-x anime">
                <hr class="bg-light left">
                <h1 class="ml9 text-center text-danger headline-xl">
                    <span class="letters">Torneos</span>
                </h1>
                <hr class="bg-light right">
            </div>
        </header>
        <?php if(!empty($terms)):
            foreach ($terms as $key => $term): ?>
                <h2 class="headline-rg text-danger headline-decorated"><?php echo getDateTransform($term->name) ?></h2>
                <div class="row tournaments-grid-wrapper webkit-scrollbar webkit-scrollbar--darker">
                    <div class="grid tournaments-grid">
                        <?php $taxonomy = [
                            [
                                'taxonomy' => 'fecha',
                                'field'    => 'term_id',
                                'terms'    => $term->term_id,
                            ]
                        ];
                        $torneos = new WP_Query( array( 'post_type' => 'torneos', 'tax_query' => $taxonomy, 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key'  => '_hora_torneo', 'order' => 'ASC' ) );
                        if($torneos->have_posts()):
                            while($torneos->have_posts()): $torneos->the_post();
                                $hora = get_post_meta($post->ID, '_hora_torneo', true);
                                $zona = get_post_meta($post->ID, '_zona_torneo', true);
                                $premio = get_post_meta($post->ID, '_premio_torneo', true); ?>
                                <div>
                                    <h3 class="headline-rg text-primary tournaments-article__title"><?php the_title(); ?></h3>
                                    <p class="text-primary excerpt-lg spaceless"><?php echo $hora; ?></p>
                                    <p class="text-info excerpt-lg"><?php echo $zona; ?></p>
                                    <p class="text-success-dark excerpt-sm excerpt-mobile-sm"><?php echo get_the_excerpt(get_the_ID()) ?></p>
                                    <p class="text-primary excerpt spaceless">Primer lugar:</p>
                                    <p class="text-wrapper text-light bg-gray"><?php echo $premio; ?></p>
                                </div>
                            <?php endwhile;
                            wp_reset_postdata();
                        endif; ?> 
                    </div>
                </div>
            <?php endforeach;
        endif; ?>
    </div>
</section>