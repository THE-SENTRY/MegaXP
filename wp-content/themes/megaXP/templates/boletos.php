<section class="bg-primary" id="tickets">
    <div class="container filler-md-bottom">
        <header class="row spacer-sm-bottom">
            <div class="section-header__divider flex flex-center-x anime">
                <hr class="bg-default left">
                <h1 class="ml9 text-center text-warning headline-xl">
                    <span class="letters">Boletos</span>
                </h1>
                <hr class="bg-default right">
            </div>
        </header>
        <div class="row text-center">
            <?php $boletos = get_page_by_path('boletos'); 
            $link_boletos = get_post_meta($boletos->ID, '_link_boletos', true);?>
            <div class="excerpt-sm spacer-md-bottom excerpt-center"><?php echo wpautop($boletos->post_content); ?></div>
            <a href="<?php echo $link_boletos; ?>"  target="_blank" class="btn btn-danger btn-primary btn-lg headline">Comprar Aquí</a>
        </div>
    </div>
</section>