<?php get_header();
	
	include( locate_template( 'templates/evento.php' ) );

	include( locate_template( 'templates/expositores.php' ) );

	include( locate_template( 'templates/invitados.php' ) );

	include( locate_template( 'templates/actividades.php' ) );

	include( locate_template( 'templates/torneos.php' ) );

	include( locate_template( 'templates/boletos.php' ) );
	
get_footer(); ?>