(function($){

	$(function(){

		var view = $(window);
		var nav = $('.navigation');
		var navScroll = 'scroll';
		var scrollEl = $('.scroll-animate');
		var navDown = 'down';
		var shrinkHeader = 300;

		view.scroll(function() {
		    var scroll = getCurrentScroll();
	      	if (scroll >= shrinkHeader) {
	           	nav.addClass(navDown);
	        } else {
	            nav.removeClass(navDown);
	        }
		});

		function getCurrentScroll() {
		    return window.pageYOffset || document.documentElement.scrollTop;
		};

		// Hide Header on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 1;
		var navbarHeight = nav.outerHeight();

		view.scroll(function(event){
		    didScroll = true;
		});

		setInterval(function() {
		    if (didScroll) {
		        hasScrolled();
		        didScroll = false;
		    }
		}, 10);

		function hasScrolled() {
		    var st = $(this).scrollTop();
		    // Make sure they scroll more than delta
		    if(Math.abs(lastScrollTop - st) <= delta)
		        return;
		    // If they scrolled down and are past the navbar, add class .nav-up.
		    // This is necessary so you never see what is "behind" the navbar.
		    if (st > lastScrollTop && st > navbarHeight){
		        // Scroll Down
		        scrollEl.addClass(navScroll);
		    } else {
		        // Scroll Up
		        if(st + $(window).height() < $(document).height()) {
		            scrollEl.removeClass(navScroll);
		        }
		    } lastScrollTop = st;
		};

		var $root = $('html, body');
		var anchor = $('.navigation__link a[href^="#"]');
		
		anchor.click(function () {
		    $root.animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top - 70
		    }, 500);
		    menu.removeClass('open');
		    return false;
		});

		var loc = window.location.href; // returns the full URL
		var blogId = $('.navigation__link--blog');
		if(/blog/.test(loc)) {
			blogId.addClass('active');
		};

	    // Wrap every letter in a span
		$('.ml9 .letters').each(function(){
			$(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
		});

		var $animation_elements = $('.anime');
		var $window = $(window);

		$window.on('scroll resize', check_if_in_view);
		$window.trigger('scroll');

		function check_if_in_view() {
			var window_height = $window.height();
			var window_top_position = $window.scrollTop() + 90;
			var window_bottom_position = (window_top_position + window_height - 120);
			$.each($animation_elements, function() {
		    	var $element = $(this);
		    	var element_height = $element.outerHeight();
		    	var element_top_position = $element.offset().top;
		    	var element_bottom_position = (element_top_position + element_height);
		    	//check to see if this current container is within viewport
		    	if ((element_bottom_position >= window_top_position) &&
		    	(element_top_position <= window_bottom_position)) {
		      		$element.addClass('in-view');
		    	} else {
		      		$element.removeClass('in-view');
		    	};
		  	});
		};

		if (!String.prototype.replaceLast) {
		    String.prototype.replaceLast = function(find, replace) {
		        var index = this.lastIndexOf(find);
		        if (index >= 0) {
		            return this.substring(0, index) + replace + this.substring(index + find.length);
		        }
		        return this.toString();
		    };
		}

		var $underlined_text = $('.header__title-underline');

		function underLineText () {
			$underlined_text.each(function(){
			    var $this = $(this);
			    var text = $this.text();
				var originalText = text;
				var breakWords = new Array();
		    	//split the words into individual strings 
			    var words = text.split(' ');
			    //find the height of the first word
			    $this.text(words[0]);
			    var height = $this.height();
		    	//if there is more than one word
		    	if (words.length > 1) {
		    		//loop through all the words
				    for(var i = 1; i < words.length; i++){
				        $this.text($this.text() + ' ' + words[i]);
		        		//check if the current word has a different height from the previous word
		        		//if this is true, we have witnessed a word wrap!
				        if($this.height() > height){
							height = $this.height();
							//add the first word after the wrap to a predefined array
							breakWords.push(words[i]); }
						//on the last iteration on the loop...
						if (i === words.length - 1) {
							if (breakWords.length > 0) {
								//select the last word of the breakWords array
								//(this is to handle cases where there there are more than one line or word wraps)
								var breakStartAt = breakWords[breakWords.length - 1];
								//add a span before the last word
								var withSpan = '<span>'+breakStartAt;
								//replace the last occurrence of this word with the span wrapped version
								//(this is to handle cases where there are more than one occurrences of the last word)
								originalText = originalText.replaceLast(breakStartAt,withSpan);
								//close the last line with a span
								originalText += "</span>";
							}
						}
					}
			    }
			    //if there are no word wraps, wrap the whole text in spans
			    else {
					originalText = '<span>'+originalText+'</span>';
				}
				//replace the original text with the span-wrapped mod
				$(this).html(originalText);
			});
		};

		$(window).load(underLineText);
	});
})(jQuery);

function toggleClassMenu() {
	menu.forEach(function(el) {
		el.classList.toggle('open');
	})
}

var menuPnl = document.querySelector('.nav');
var menuBtn = document.querySelector('.btn-nav');
var menu = [menuPnl, menuBtn];
menu.forEach(function(el) {
	el.addEventListener("click", toggleClassMenu, false);
})
