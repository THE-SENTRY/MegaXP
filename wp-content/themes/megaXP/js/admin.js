(function($){

  	"use strict";
	
  	$(function(){
		
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd' 
		});

		/**	
		 * DIRECCION COLABORADORES
		 */
		if (document.getElementById("addresscolaborador")) {
			var autocomplete = new google.maps.places.Autocomplete($("#addresscolaborador")[0], {});

	      google.maps.event.addListener(autocomplete, 'place_changed', function() {
	     	 	var place = autocomplete.getPlace();

	     	 	$('#lat_evento').val( place.geometry.location.lat() );
	     	 	$('#long_evento').val( place.geometry.location.lng() );

	     	 	var iframe = '<iframe width="100%" height="170" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+place.geometry.location.lat()+','+place.geometry.location.lng()+'&hl=es;z=14&amp;output=embed"></iframe>';
	     	 	$('.iframe-cont').empty().append(iframe);

	      });
		};

		// Uploading files image mobile home
		var file_frame;

		$.fn.upload_listing_image = function( button ) {
			var button_id = button.attr('id');
			var field_id = button_id.replace( '_button', '' );

			// If the media frame already exists, reopen it.
			if ( file_frame ) {
			  file_frame.open();
			  return;
			}

			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media({
			  title: $( this ).data( 'uploader_title' ),
			  button: {
			    text: $( this ).data( 'uploader_button_text' ),
			  },
			  multiple: false
			});

			// When an image is selected, run a callback.
			file_frame.on( 'select', function() {
			  var attachment = file_frame.state().get('selection').first().toJSON();
			  $("#"+field_id).val(attachment.id);
			  $("#content-img-home img").attr('src',attachment.url).attr('srcset', attachment.url);
			  $( '#content-img-home img' ).show();
			  $( '#' + button_id ).attr( 'id', 'remove_listing_image_button' );
			  $( '#remove_listing_image_button' ).text( 'Eliminar imagen destacada' );
			});

			// Finally, open the modal
			file_frame.open();
		};

		$(document).on( 'click', '#upload_listing_image_button', function( event ) {
			event.preventDefault();
			$.fn.upload_listing_image( $(this) );
		});

		$(document).on( 'click', '#remove_listing_image_button', function( event ) {
			event.preventDefault();
			$( '#upload_listing_image' ).val( '' );
			$( '#content-img-home img' ).attr( 'src', '' ).attr('srcset', '');
			$( '#content-img-home img' ).hide();
			$( this ).attr( 'id', 'upload_listing_image_button' );
			$( '#upload_listing_image_button' ).text( 'Añadir imagen destacada' );
		});

	
  	});

})(jQuery);