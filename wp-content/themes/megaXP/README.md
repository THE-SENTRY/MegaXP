WordPress Base Theme
=============
![Alejandro cervantes]()

Usar para iniciar un nuevo tema de WordPress

### Credits

Link: http://alejandrocervantes.me/<br />
Contributors: THE-SENTRY, chokonet, MaximoSL<br />
Tags: wordpress, themes<br />
Tested up to: 3.8<br />
Stable tag: 1.0<br />
License: GPLv2 or later<br />
License URI: http://www.gnu.org/licenses/gpl-2.0.html<br />
