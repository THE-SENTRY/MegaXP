<?php
require 'inc/functions-initial-setup.php';
require 'inc/functions-helpers.php';
require 'inc/functions-pages.php';
require 'inc/functions-post-types.php';
require 'inc/functions-metaboxes.php';
require 'inc/taxonomies.php';