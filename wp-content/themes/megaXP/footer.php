		<footer class="footer">
			<div class="bg-dark">
				<div class="container">
					<div class="row flex flex-center-x footer__wrapper">
						<div class="col-xs-12 col-md-6 footer__article flex flex-center-x">
							<div class="footer__mail-image">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icons/envelope.svg" alt="mail image">
							</div>
							<h3 class="headline-rg text-warning spaceless">Inscribete en nuestro newsletter</h3>
						</div>
						<form action="https://megaxp.us17.list-manage.com/subscribe/post?u=2fb6035e3d65ffb5a0c49fb33&amp;id=f9ca1331a7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="col-xs-12 col-md-6 validate" target="_blank" novalidate>
							<div class="input-group input-group-lg">
								<input type="email" class="form-control text-dark footer__input required email" name="EMAIL" id="mce-EMAIL">
								
							    <div style="position: absolute; left: -5000px;" aria-hidden="true">
							    	<input type="text" name="b_2fb6035e3d65ffb5a0c49fb33_f9ca1331a7" tabindex="-1" value="">
							    </div>
								<span class="input-group-btn">
									<button class="btn btn-warning headline footer__btn" id="mc-embedded-subscribe">Suscribirme</button>
								</span>
							</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none">asd</div>
								<div class="response" id="mce-success-response" style="display:none">asd asd</div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							
						</form>
					</div>
				</div>
			</div>
			<div class="bg-darker">
				<div class="container text-white footer__basement">
					<p class="footer__copyright text-uppercase">&#169;2017 MegaXP. All rights reserved.</p>
					<ul class="list-inline spaceless">
						<li>
							<ul class="list-inline footer__breadcrumb hidden-xs hidden-sm text-uppercase">
								<li>
									<a href="<?php echo site_url('/blog') ?>">Blog</a>
								</li>
								<li>
									<a href="<?php echo site_url('/aviso-de-privacidad') ?>">Aviso de privacidad</a>
								</li>
							</ul>
						</li>
						<li class="footer__social-wrapper">
							<ul class="list-inline">
								<li>
									<p>Siguenos en</p>
								</li>
								<li>
									<a href="https://www.facebook.com/megaxpoficial/" target="_blank" class="footer__social">
										<i data-icon="&#xf308"></i>
									</a>
								</li>
								<li>
									<a href="https://twitter.com/megaxpoficial" target="_blank" class="footer__social">
										<i data-icon="&#xf304"></i>
									</a>
								</li>
								<li>
									<a href="https://www.instagram.com/megaxp/" target="_blank" class="footer__social">
										<i data-icon="&#xf16d"></i>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</footer>
		<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
		<?php wp_footer(); ?>
	</body>

</html>
