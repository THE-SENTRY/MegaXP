<?php get_header(); 
$pagina = isset($_GET['pagina']) ? $_GET['pagina'] : 1;
$offset = ($pagina - 1) * 10;?>

	<section class="container fixed-spacer blog-container">
		<h2 class="headline-sm text-center bg-primary text-white blog-index-headline">No te pierdas ninguna de nuestras novedades, aquí tenemos las notas mas nuevas del mundo de los juegos de mesa</h2>
    	<div class="row">
			<?php $blog = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 10, 'offset' => $offset ) );
			if($blog->have_posts()):
				while($blog->have_posts()): $blog->the_post();
					$image_url = attachment_image_url(get_the_ID(), 'single'); ?>
		            <div class="col-ss-12 col-xs-6 col-md-12 hover-mask article-hover">
    					<a href="<?php echo get_permalink(); ?>" class="row show">
    						<img src="<?php echo $image_url; ?>" alt="article image" class="col-xs-12 col-md-5 img-crop blog-index__col">
    						<div class="col-xs-12 col-md-7 blog-index__col">
    							<h2 class="headline-lg text-danger blog-index__title"><?php the_title(); ?></h2>
    							<div class="excerpt text-darker blog-index__excerpt">
    								<?php the_excerpt(); ?>
    							</div>
    							<p class="excerpt text-info blog-index__link">Seguir leyendo...</p>
    						</div>
    					</a>
    					<ul class="row headline blog__list blog-index__list text-primary">
    						<li class="col-md-7">
    							<i data-icon="&#xe801"></i>
								<?php $terms = get_the_terms( get_the_ID(), 'category' );
                     
								if ( $terms && ! is_wp_error( $terms ) ) : 
								 
								    $draught_links = array();
								 
								    foreach ( $terms as $term ) {
								        $draught_links[] = $term->name;
								    }
								                         
								    $on_draught = join( ", ", $draught_links );
								    ?>
								 
								    <?php printf( esc_html__( '%s', 'textdomain' ), esc_html( $on_draught ) ); ?>
								<?php endif; ?>

    						</li>
    						<li class="col-md-3">
    							<i data-icon="&#xe800"></i>Por: <?php echo get_the_author(); ?>
    						</li>
    						<li class="col-md-2">
    							<p class="spaceless"><?php the_date('d/m/Y', '<time>', '</time>'); ?></p>
    						</li>
    					</ul>
    				</div>
				<?php endwhile;
				wp_reset_postdata();
			endif; ?>
		</div>
		<?php if($blog->max_num_pages > 1):
			$url = site_url('/blog/');
			pagenavi($pagina, $blog->max_num_pages, $url, true);  
		endif; ?>
		
	</section>

<?php get_footer(); ?>