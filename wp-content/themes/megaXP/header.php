<!doctype html>
	<head>
		<meta charset="utf-8">
		<title><?php print_title(); ?></title>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#285193">
		<meta http-equiv="cleartype" content="on">
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<?php wp_head(); ?>
	</head>

	<body data-spy="scroll" data-target="#nav-wrapper" data-offset="75" <?php body_class(); ?>>
		<!--[if lt IE 9]>
			<p class="chromeframe">Estás usando una versión <strong>vieja</strong> de tu explorador. Por favor <a href="http://browsehappy.com/" target="_blank"> actualiza tu explorador</a> para tener una experiencia completa.</p>
		<![endif]-->
		
	    <nav class="navigation navbar navbar-fixed-top container-fluid scroll-animate">
			<div class="overflow-container" id="nav-wrapper">
				<div class="navbar-header">
					<a href="/" class="navigation__logo">
			            <picture>
	          				<source media="(min-width: 768px)" srcset="<?php echo get_template_directory_uri(); ?>/images/logo.png">
		            		<img src="<?php echo get_template_directory_uri(); ?>/images/logo-horizontal.png" alt="MegaXP">
		            	</picture>
			        </a>
					<button type="button" class="btn-nav">
						<span class="btn-nav__bar"></span>
					</button>
				</div>
				<ul class="navbar-nav nav navbar-right headline text-white">
		            <li class="navigation__link">
		            	<a href="<?php echo site_url('/'); ?>">Home</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#event">Evento</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#exhibitors">Expositores</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#guests">Invitados</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#activities">Eventos</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#tournaments">Torneos</a>
		            </li>
		            <li class="navigation__link">
		            	<a href="<?php echo !is_home() ? site_url('/') : '' ?>#tickets">Boletos</a>
		            </li>
		            <li class="navigation__link navigation__link--blog">
		            	<a href="<?php echo site_url('/blog'); ?>">Blog</a>
		            </li>
		        </ul>
			</div>
	    </nav>
	    <?php if(is_home()): ?>
		    <main id="home" name="home" class="fixed-spacer">
		    	<?php $home = get_page_by_path('header-home');
	    		$subtitle_home = get_post_meta($home->ID, '_subtitle_home', true);
	    		$image_url = attachment_image_url($home->ID, 'home_header');
	    		$image_url_mobile = imageMobileHome($home->ID); ?>
	    		<style scoped>
					.header {
					   background-image: url('<?php echo $image_url ?>');
					}
					@media (max-width: 570px) {
					    .header {
					   		background-image: url('<?php echo $image_url_mobile ?>');
						}
					}
				</style>
		    	<header class="header text-center flex flex-center">
			    	<h1 class="text-white headline-antialiasing header__title-underline"><?php echo $subtitle_home; ?></h1>
				    <p class="text-warning excerpt-lg excerpt-light excerpt-mobile-sm"><?php echo $home->post_content; ?></p>
				</header>
		    </main>
		<?php endif; ?>