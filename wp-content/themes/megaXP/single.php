<?php get_header(); the_post();  
	$image_url = attachment_image_url(get_the_ID(), 'single'); 
	$author_id = $post->post_author; ?>
	<section class="container fixed-spacer blog-container">
    	<div class="row">
    		<h1 class="col-sm-12 headline-md blog-title"><?php the_title(); ?></h1>
    		<div class="col-sm-8">
    			<img src="<?php echo $image_url ?>" alt="article cover" class="spacer-md-bottom blog__image img-crop">
    			<div class="blog-content text-darker">
    				<?php the_content() ?>
    			</div>
    			<h4 class="headline-sm text-info blog-wrapper-title">Te puede interesar</h4>
    			<div class="row blog-wrapper">
    				<?php $blog = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 4, 'post__not_in' => [get_the_ID()], 'orderby' =>'rand' ) );
					if($blog->have_posts()):
						while($blog->have_posts()): $blog->the_post(); 
							$image_url = attachment_image_url(get_the_ID(), 'archive'); ?>
				            <article class="col-ss-12 col-xs-4 blog-article article-hover">
		    					<a href="<?php echo get_permalink(); ?>" class="undecorated">
		    						<h4 class="headline-sm blog-title"><?php the_title(); ?></h4>
			    					<img src="<?php echo $image_url; ?>" alt="article image" class="article-image blog-article__image">
		    					</a>
		    					<ul class="row headline blog__list blog-article__list">
		    						<li class="col-xs-8">
		    							<a href="#"><i data-icon="&#xe800"></i>Por: <?php echo get_the_author(); ?></a>
		    						</li>
		    						<li class="col-xs-4">
		    							<?php the_date('d/m/Y', '<time>', '</time>'); ?>
		    						</li>
		    					</ul>
		    				</article>
						<?php endwhile;
						wp_reset_postdata();
					endif; ?>
    			</div>
    		</div>
    		<aside class="col-sm-4 blog-sidebar hidden-xs scroll-animate">
    			<div>
    				<h4 class="headline text-center text-light">Sobre el autor</h4>
					<div class="text-center blog-sidebar__author">
						<h4 class="headline text-light"><?php echo get_the_author(); ?></h4>
						<p class="excerpt-xs text-darker"><?php the_author_meta( 'description', $author_id ); ?></p>
					</div>
    				<h4 class="headline text-center text-light">Mas del autor</h4>
    				<?php $blogsuthor = new WP_Query( array( 'post_type' => 'post', 'author' => $author_id, 'post__not_in' => [get_the_ID()] ) );
					if($blogsuthor->have_posts()):
						while($blogsuthor->have_posts()): $blogsuthor->the_post(); 
							$image_url = attachment_image_url(get_the_ID(), 'archive'); ?>
		    				<a href="<?php echo get_permalink(); ?>" class="undecorated article-hover">
								<h4 class="headline-sm blog-title"><?php the_title(); ?></h4>
		    					<img src="<?php echo $image_url; ?>" alt="article image" class="article-image blog-article__image">
							</a>
						<?php endwhile;
						wp_reset_postdata();
					endif; ?>
    			</div>
    		</aside>
    	</div>
    </section>

<?php get_footer(); ?>