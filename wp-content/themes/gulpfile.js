var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var minify = require('gulp-minify-css');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "megaxp.localhost"
    });
});

gulp.task('css', function(){
   gulp.src('megaxp/css/style.css')
   .pipe(minify())
   .pipe(gulp.dest('megaxp'));
});

gulp.task('default',['css', 'browser-sync'],function(){
});